from flask import Flask, flash, redirect, url_for, render_template, g, session
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object('config')

db = SQLAlchemy(app)

@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

from app.users.models import User
@app.before_request
def load_user():
  g.user = None
  if 'user_id' in session:
    g.user = User.query.get(session['user_id']);


@app.route('/')
def portal():
  flash('Welcome to RSSR')
  return redirect(url_for('users.login'))
  #return redirect(url_for('feed.home'))



from app.users.views import mod as usersModule
app.register_blueprint(usersModule)

from app.feed.views import mod as feedModule
app.register_blueprint(feedModule)

