#!/usr/bin/env python
import os
import readline
from pprint import pprint

from flask import *
from app import *
from app.users.models import *
from flask_oauth import OAuth

import tweepy, feedparser

os.environ['PYTHONINSPECT'] = 'True'
